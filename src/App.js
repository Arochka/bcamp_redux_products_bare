import React, { Component } from "react"
import axios from "axios"
import AppList from "./feature/list/app-list"
import AppPrice from "./feature/price/app-price"

import "./App.css"

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            productList: [],
            totalPrice: 0
        }

        this.getProducts()
    }

    render() {
        return (
            <main className="AppMain">
                <section className="AppMain__column">
                    <AppList
                        productList={this.state.productList}
                        selectProduct={this.selectProduct}
                    />
                </section>

                <section className="AppMain__column">
                    <AppPrice
                        productList={this.state.productList}
                        totalPrice={this.state.totalPrice}
                    />
                </section>
            </main>
        )
    }

    getProducts = () => {
        axios.get("http://localhost:9000/products").then(productList => {
            this.setState({ productList: productList.data })
        })
    }

    selectProduct = product => {
        product.selected = !product.selected
        const totalPrice = this.state.productList.reduce((acc, product) => {
            if (product.selected) {
                return (acc += product.price)
            }
            return acc
        }, 0)

        this.setState({ productList: this.state.productList, totalPrice })
    }
}

export default App
