import React, { Component } from "react"

import ProductList from "./components/product-list"

export class AppList extends Component {
    componentDidMount() {}

    render() {
        return (
            <ProductList
                productList={this.props.productList}
                selectProduct={this.props.selectProduct}
            />
        )
    }
}

export default AppList
