import React, { Component } from "react"

import PriceHeader from "./components/price-header"
import PriceList from "./components/price-list"

import "./price-list.css"

export class AppPrice extends Component {
    render() {
        return (
            <div className="Wrapper">
                <PriceHeader totalPrice={this.props.totalPrice} />
                <PriceList
                    products={this.props.productList.filter(
                        product => product.selected
                    )}
                />
            </div>
        )
    }
}

export default AppPrice
