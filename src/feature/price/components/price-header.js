import React from "react"

const PriceHeader = props => {
    return (
        <article className="TotalPrice">
            <h2 className="TotalPrice__Title">Total</h2>
            <span className="TotalPrice__Value">{props.totalPrice} &euro;</span>
        </article>
    )
}

export default PriceHeader
