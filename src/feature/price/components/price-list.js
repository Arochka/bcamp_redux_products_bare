import React from "react"

const PriceList = props => {
    const items = props.products.map(product => {
        return (
            <li key={product.name} className="PriceList__Item">
                <strong className="PriceList__Item__Name">
                    {product.name}
                </strong>
                <span className="PriceList__Item__Price">
                    {product.price} &euro;
                </span>
            </li>
        )
    })

    return <ul className="PriceList">{items}</ul>
}

export default PriceList
